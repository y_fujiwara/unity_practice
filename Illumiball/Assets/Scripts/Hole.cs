﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {

	#region ■メンバ変数■
	/// <summary>
    /// どのボールを吸い寄せるかを判別するタグ
    /// </summary>
	public string activeTag;

	/// <summary>
    /// 穴に入ったかのフラグ用変数
    /// </summary>
	private bool fallIn;
	#endregion

	#region ■Publicメソッド■
	/// <summary>
    /// ボールが入っているかを返す
    /// </summary>
    /// <returns></returns>
	public bool IsFallIn() {
		return fallIn;
	}
	#endregion

	#region ■Privateメソッド■
	/// <summary>
	/// コリジョンイベントその１
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	private void OnTriggerEnter(Collider other)
	{
		this.fallIn = (other.gameObject.tag == activeTag) ? true : this.fallIn;
	}

	/// <summary>
	/// コリジョンイベントその２
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	private void OnTriggerExit(Collider other)
	{
		this.fallIn = (other.gameObject.tag == activeTag) ? false : this.fallIn;
	}

	/// <summary>
    /// 衝突検知用メソッド
    /// </summary>
    /// <param name="other"></param>
	private void OnTriggerStar (Collider other) {
		// コライダに触れているオブジェクトのRigidbodyコンポーネントを取得
		Rigidbody rigidbody = other.gameObject.GetComponent<Rigidbody>();

		// ボールがどの方向にあるかを計算
		Vector3 direction = transform.position - other.gameObject.transform.position;
		direction.Normalize(); // Normalizeは副作用あり

		// タグに応じた処理わけ
		if (other.gameObject.tag == activeTag) {
			// 中心地点でボールを止めるため速度を減速させる
			rigidbody.velocity *= 0.9f;
			rigidbody.AddForce(direction * rigidbody.mass * 20.0f); // massは重さ
		}
		else {
			rigidbody.AddForce(-direction * rigidbody.mass * 80.0f);
		}
	}
	#endregion
}
