﻿using UnityEngine;
using System.Collections;

public class FallInChecker : MonoBehaviour {
	#region ■メンバ変数■
	public Hole red;
	public Hole blue;
	public Hole green;
	#endregion

	#region ■Privateメソッド■
	/// <summary>
	/// OnGUI is called for rendering and handling GUI events.
	/// This function can be called multiple times per frame (one call per event).
	/// </summary>
	void OnGUI()
	{
		string label = " ";
		// 全てのボールが入ったらラベルを表示
		label = this.IsAllFallIn() ? "Fall in hole!" : " ";

		GUI.Label(new Rect(0, 0, 100, 30), label);
	}

	/// <summary>
    /// クリア条件判定用メソッド
    /// </summary>
    /// <returns></returns>
	private bool IsAllFallIn() {
		if (this.red.IsFallIn() && this.blue.IsFallIn() && this.green.IsFallIn() ) {
			return true;
		}
		return false;
	}
	#endregion

	
}
