﻿using UnityEngine;
using System.Collections;

public class GravityController : MonoBehaviour {

	/// <summary>
	/// 重力加速度
	/// </summary>	
	private const float Gravity = 9.81f;

	/// <summary>
	/// 重力の適用具合
	/// </summary>
	public float gravityScale = 1.0f;

	// Use this for initialization
	private void Start () {
	
	}
	
	/// <summary>
	/// フレーム単位で実行されるUpdateメソッド
	/// </summary>
	private void Update () {
		Vector3 vector = new Vector3();

		// 構造体なので参照を渡して中身を変更するような方法はNG
		vector = Application.isEditor ? this.CreateEditorVector() : this.CreateMobileVector();

		// シーンの重力を入力ベクトルの方向ん居合わせて変化させる
		Physics.gravity = Gravity * vector.normalized * gravityScale;
	}

	/// <summary>
    /// Unityエディタで操作するときのベクター生成
    /// </summary>
    /// <returns>Vector3構造体</returns>
	private Vector3 CreateEditorVector() {
		Vector3 localVec = new Vector3(); 
		// キー入力の受付 
		localVec.x = Input.GetAxis("Horizontal"); 
		localVec.z = Input.GetAxis("Vertical"); 
		// 高さ方向の判定はキーのzとする 
		localVec.y = Input.GetKey("z") ? 1.0f : -1.0f;

		return localVec;
	}

	/// <summary>
    /// スマホで操作するときのベクター生成
    /// </summary>
    /// <returns>Vector3構造体</returns>
	private Vector3 CreateMobileVector() {
		Vector3 localVec = new Vector3();

		// 加速度センサーの入力をUnity空間の軸にマッピングする
		localVec.x = Input.acceleration.x;
		localVec.y = Input.acceleration.y;
		localVec.z = Input.acceleration.z;

		return localVec;
	}
}
